from flask import jsonify
from http import HTTPStatus

def createHotel(conn, data):
  if 'hotel_name' not in data or 'address' not in data:
    return jsonify({
        'result': None
        }), HTTPStatus.BAD_REQUEST

  try:
    with conn.cursor() as cursor:
      sql = 'INSERT INTO `hotel` (`hotel_name`, `address`) VALUES (%s, %s)'
      cursor.execute(sql, (data['hotel_name'], data['address']))
      conn.commit()
      hotel_id = cursor.lastrowid
    return jsonify({
        'result': 'Success',
        'id': hotel_id
        }), HTTPStatus.OK
  except:
    print('ERROR: Create hotel')
    return jsonify({
        'result': None
        }), HTTPStatus.INTERNAL_SERVER_ERROR

def readHotel(conn):
  try:
    with conn.cursor() as cursor:
      sql = 'SELECT * FROM hotel'
      cursor.execute(sql)
      result = cursor.fetchall()
    return jsonify({
        'result': result
        }), HTTPStatus.OK
  except:
    print('ERROR: Read hotel')
    return jsonify({
        'result': None
        }), HTTPStatus.INTERNAL_SERVER_ERROR

def updateHotel(conn, data):
  if 'id' not in data or 'hotel_name' not in data or 'address' not in data:
    return jsonify({
        'result': None
        }), HTTPStatus.BAD_REQUEST

  try:
    with conn.cursor() as cursor:
      sql = 'UPDATE hotel ' + \
          'SET hotel_name = %s, address = %s ' + \
          'WHERE id = %s'
      cursor.execute(sql, (data['hotel_name'], data['address'], data['id']))
      conn.commit()
    return jsonify({
        'result': 'Success'
        }), HTTPStatus.OK
  except:
    print('ERROR: Update hotel')
    return jsonify({
        'result': None
        }), HTTPStatus.INTERNAL_SERVER_ERROR

def deleteHotel(conn, data):
  if 'id' not in data:
    return jsonify({
        'result': None
        }), HTTPStatus.BAD_REQUEST

  try:
    with conn.cursor() as cursor:
      sql = 'DELETE FROM stayroom ' + \
          'WHERE room_id IN ( ' + \
          'SELECT id FROM room WHERE hotel_id = %s ' + \
          ')'
      cursor.execute(sql, (data['id']))
      conn.commit()

      sql = 'DELETE FROM stay ' + \
          'WHERE room_id IN ( ' + \
          'SELECT id FROM room WHERE hotel_id = %s ' + \
          ')'
      cursor.execute(sql, (data['id']))
      conn.commit()

      sql = 'DELETE FROM reservation ' + \
          'WHERE room_id IN ( ' + \
          'SELECT id FROM room WHERE hotel_id = %s ' + \
          ')'
      cursor.execute(sql, (data['id']))
      conn.commit()

      sql = 'DELETE FROM hotel WHERE id = %s'
      cursor.execute(sql, (data['id']))
      conn.commit()
    return jsonify({
        'result': 'Success'
        }), HTTPStatus.OK
  except:
    print('ERROR: Delete hotel')
    return jsonify({
        'result': None
        }), HTTPStatus.INTERNAL_SERVER_ERROR
