from datetime import datetime, timedelta
from flask import jsonify
from http import HTTPStatus

def createReservation(conn, data):
  if 'hotel_id' not in data or 'room_id' not in data or \
      'customer_name' not in data or 'guest_name' not in data or \
      'checkin_date' not in data or 'checkout_date' not in data or \
      'order_id' not in data:
    return jsonify({
        'result': None
        }), HTTPStatus.BAD_REQUEST

  try:
    with conn.cursor() as cursor:
      sql = 'INSERT INTO `reservation` (`order_id`, `hotel_id`, `room_id`, ' + \
          '`customer_name`, `checkin_date`, `checkout_date`) ' + \
          'VALUES (%s, %s, %s, %s, %s, %s)'
      cursor.execute(sql, (data['order_id'], data['hotel_id'], data['room_id'],
          data['customer_name'], data['checkin_date'], data['checkout_date']))
      conn.commit()
      reservation_id = cursor.lastrowid

      sql = 'INSERT INTO `stay` (`reservation_id`, `guest_name`, `room_id`) ' + \
          'VALUES (%s, %s, %s)'
      cursor.execute(sql, (reservation_id, data['guest_name'], data['room_id']))
      conn.commit()
      stay_id = cursor.lastrowid

      checkin = datetime.strptime(data['checkin_date'], '%Y-%m-%d')
      checkout = datetime.strptime(data['checkout_date'], '%Y-%m-%d')
      for i in range((checkout - checkin).days):
        sql = 'INSERT INTO `stayroom` (`stay_id`, `room_id`, `date`) ' + \
            'VALUES (%s, %s, %s)'
        cursor.execute(sql, (stay_id, data['room_id'], (checkin + timedelta(days=i)).strftime('%Y-%m-%d')))
        conn.commit()

    return jsonify({
        'result': 'Success',
        'id': reservation_id
        }), HTTPStatus.OK
  except:
    print('ERROR: Create reservation')
    return jsonify({
        'result': None
        }), HTTPStatus.INTERNAL_SERVER_ERROR

def readReservation(conn):
  try:
    with conn.cursor() as cursor:
      sql = 'SELECT ' + \
          'reservation.id, reservation.order_id, reservation.customer_name, ' + \
          'stay.guest_name, reservation.hotel_id, hotel.hotel_name, ' + \
          'room.room_number, reservation.checkin_date, ' + \
          'reservation.checkout_date ' + \
          'FROM reservation ' + \
          'LEFT JOIN hotel ON reservation.hotel_id = hotel.id ' + \
          'LEFT JOIN room ON reservation.room_id = room.id ' + \
          'LEFT JOIN stay ON reservation.id = stay.reservation_id ' + \
          'ORDER BY reservation.checkin_date ASC'
      cursor.execute(sql)
      result = cursor.fetchall()
    return jsonify({
        'result': result
        }), HTTPStatus.OK
  except:
    print('ERROR: Read reservation')
    return jsonify({
        'result': None
        }), HTTPStatus.INTERNAL_SERVER_ERROR

def updateReservation(conn, data):
  if 'hotel_id' not in data or 'room_id' not in data or \
      'customer_name' not in data or 'guest_name' not in data or \
      'checkin_date' not in data or 'checkout_date' not in data or \
      'order_id' not in data or 'id' not in data:
    return jsonify({
        'result': None
        }), HTTPStatus.BAD_REQUEST

  try:
    with conn.cursor() as cursor:
      sql = 'DELETE FROM stayroom ' + \
          'WHERE stay_id IN ( ' + \
          'SELECT id FROM stay WHERE reservation_id = %s ' + \
          ')'
      cursor.execute(sql, (data['id']))
      conn.commit()

      sql = 'UPDATE reservation ' + \
          'SET order_id = %s, hotel_id = %s, room_id = %s, ' + \
          'customer_name = %s, checkin_date = %s, checkout_date = %s ' + \
          'WHERE id = %s'
      cursor.execute(sql, (data['order_id'], data['hotel_id'], data['room_id'],
          data['customer_name'], data['checkin_date'], data['checkout_date'],
          data['id']))
      conn.commit()

      sql = 'UPDATE stay ' + \
          'SET guest_name = %s, room_id = %s ' + \
          'WHERE reservation_id = %s'
      cursor.execute(sql, (data['guest_name'], data['room_id'], data['id']))
      conn.commit()

      sql = 'SELECT id FROM stay ' + \
          'WHERE reservation_id = %s'
      cursor.execute(sql, (data['id']))
      result = cursor.fetchone()
      stay_id = result['id']

      checkin = datetime.strptime(data['checkin_date'], '%Y-%m-%d')
      checkout = datetime.strptime(data['checkout_date'], '%Y-%m-%d')
      for i in range((checkout - checkin).days):
        sql = 'INSERT INTO stayroom (stay_id, room_id, date) ' + \
            'VALUES (%s, %s, %s)'
        cursor.execute(sql, (stay_id, data['room_id'], (checkin + timedelta(days=i)).strftime('%Y-%m-%d')))
        conn.commit()

    return jsonify({
        'result': 'Success'
        }), HTTPStatus.OK
  except:
    print('ERROR: Update reservation')
    return jsonify({
        'result': None
        }), HTTPStatus.INTERNAL_SERVER_ERROR

def deleteReservation(conn, data):
  if 'id' not in data:
    return jsonify({
        'result': None
        }), HTTPStatus.BAD_REQUEST

  try:
    with conn.cursor() as cursor:
      sql = 'DELETE FROM stayroom ' + \
          'WHERE stay_id IN ( ' + \
          'SELECT id FROM stay WHERE reservation_id = %s ' + \
          ')'
      cursor.execute(sql, (data['id']))
      conn.commit()

      sql = 'DELETE FROM stay WHERE reservation_id = %s'
      cursor.execute(sql, (data['id']))
      conn.commit()

      sql = 'DELETE FROM reservation WHERE id = %s'
      cursor.execute(sql, (data['id']))
      conn.commit()

    return jsonify({
        'result': 'Success'
        }), HTTPStatus.OK
  except:
    print('ERROR: Delete reservation')
    return jsonify({
        'result': None
        }), HTTPStatus.INTERNAL_SERVER_ERROR
