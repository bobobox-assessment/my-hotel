def getConfig():
  host = ''
  config = ''
  with open('db.conf', 'r') as f:
    # read db file config
    host = f.readline().strip('\n').strip()
    config = f.readline().strip('\n').strip()

  host = host.split(':')
  config = config.split(':')

  if len(host) == 2 and len(config) == 2:
    return (host[0], host[1], config[0], config[1])

  return None
