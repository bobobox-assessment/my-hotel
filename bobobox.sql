-- --------------------------------------------------------
-- Host:                         192.168.100.56
-- Server version:               8.0.21 - MySQL Community Server - GPL
-- Server OS:                    Linux
-- HeidiSQL Version:             11.0.0.5919
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Dumping database structure for bobobox
CREATE DATABASE IF NOT EXISTS `bobobox` /*!40100 DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci */ /*!80016 DEFAULT ENCRYPTION='N' */;
USE `bobobox`;

-- Dumping structure for table bobobox.hotel
CREATE TABLE IF NOT EXISTS `hotel` (
  `id` int NOT NULL AUTO_INCREMENT,
  `hotel_name` varchar(50) NOT NULL,
  `address` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- Dumping data for table bobobox.hotel: ~0 rows (approximately)
/*!40000 ALTER TABLE `hotel` DISABLE KEYS */;
INSERT INTO `hotel` (`id`, `hotel_name`, `address`) VALUES
	(1, 'Hotel 1', 'jalan hotel 1'),
	(2, 'Hotel 2', 'jalan hotel 2'),
	(3, 'Hotel 3', 'jalan hotel 3'),
	(4, 'Hotel 4', 'jalan hotel 4');
/*!40000 ALTER TABLE `hotel` ENABLE KEYS */;

-- Dumping structure for table bobobox.reservation
CREATE TABLE IF NOT EXISTS `reservation` (
  `id` int NOT NULL AUTO_INCREMENT,
  `order_id` int NOT NULL,
  `room_id` int NOT NULL,
  `customer_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL DEFAULT '',
  `checkin_date` date NOT NULL,
  `checkout_date` date NOT NULL,
  `hotel_id` int NOT NULL,
  PRIMARY KEY (`id`),
  KEY `RESERVATION_HOTEL` (`hotel_id`),
  KEY `RESERVATION_ROOM` (`room_id`),
  CONSTRAINT `RESERVATION_HOTEL` FOREIGN KEY (`hotel_id`) REFERENCES `hotel` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `RESERVATION_ROOM` FOREIGN KEY (`room_id`) REFERENCES `room` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- Dumping data for table bobobox.reservation: ~0 rows (approximately)
/*!40000 ALTER TABLE `reservation` DISABLE KEYS */;
INSERT INTO `reservation` (`id`, `order_id`, `room_id`, `customer_name`, `checkin_date`, `checkout_date`, `hotel_id`) VALUES
	(1, 1001, 16, 'Customer 1', '2020-01-11', '2020-01-13', 3),
	(2, 1002, 7, 'Customer 2', '2020-01-10', '2020-01-11', 2),
	(3, 1003, 17, 'Customer 3', '2020-01-14', '2020-01-16', 3),
	(4, 1004, 2, 'Customer 4', '2020-01-10', '2020-01-12', 1),
	(5, 1005, 10, 'Customer 5', '2020-01-10', '2020-01-11', 2),
	(6, 1006, 15, 'Customer 6', '2020-01-19', '2020-01-20', 3),
	(9, 1007, 1, 'Customer 7', '2020-01-09', '2020-01-10', 1),
	(10, 1008, 1, 'Customer 8', '2020-01-20', '2020-01-22', 1),
	(11, 1009, 14, 'Customer 9', '2020-01-08', '2020-01-11', 3);
/*!40000 ALTER TABLE `reservation` ENABLE KEYS */;

-- Dumping structure for table bobobox.room
CREATE TABLE IF NOT EXISTS `room` (
  `id` int NOT NULL AUTO_INCREMENT,
  `hotel_id` int NOT NULL,
  `room_number` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `room_status` tinyint NOT NULL DEFAULT '1' COMMENT '1: available, 2: out of service',
  PRIMARY KEY (`id`),
  KEY `ROOM_HOTEL` (`hotel_id`),
  CONSTRAINT `ROOM_HOTEL` FOREIGN KEY (`hotel_id`) REFERENCES `hotel` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- Dumping data for table bobobox.room: ~0 rows (approximately)
/*!40000 ALTER TABLE `room` DISABLE KEYS */;
INSERT INTO `room` (`id`, `hotel_id`, `room_number`, `room_status`) VALUES
	(1, 1, '301', 1),
	(2, 1, '302', 1),
	(3, 1, '303', 1),
	(4, 1, '501', 1),
	(5, 1, '502', 1),
	(6, 1, '503', 1),
	(7, 2, '301', 1),
	(8, 2, '302', 1),
	(9, 2, '303', 1),
	(10, 2, '501', 1),
	(11, 2, '502', 2),
	(12, 2, '503', 1),
	(13, 3, '301', 1),
	(14, 3, '302', 1),
	(15, 3, '303', 1),
	(16, 3, '501', 1),
	(17, 3, '502', 1),
	(18, 3, '503', 1);
/*!40000 ALTER TABLE `room` ENABLE KEYS */;

-- Dumping structure for table bobobox.stay
CREATE TABLE IF NOT EXISTS `stay` (
  `id` int NOT NULL AUTO_INCREMENT,
  `reservation_id` int NOT NULL,
  `room_id` int NOT NULL,
  `guest_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `STAY_RESERVATION` (`reservation_id`),
  KEY `STAY_ROOM` (`room_id`),
  CONSTRAINT `STAY_RESERVATION` FOREIGN KEY (`reservation_id`) REFERENCES `reservation` (`id`),
  CONSTRAINT `STAY_ROOM` FOREIGN KEY (`room_id`) REFERENCES `room` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- Dumping data for table bobobox.stay: ~0 rows (approximately)
/*!40000 ALTER TABLE `stay` DISABLE KEYS */;
INSERT INTO `stay` (`id`, `reservation_id`, `room_id`, `guest_name`) VALUES
	(1, 1, 16, 'Guest 1'),
	(2, 2, 7, 'Guest 2'),
	(3, 3, 17, 'Guest 3'),
	(4, 4, 2, 'Guest 4'),
	(5, 5, 10, 'Guest 5'),
	(6, 6, 15, 'Guest 6'),
	(8, 9, 1, 'Guest 7'),
	(9, 10, 1, 'Guest 8'),
	(10, 11, 14, 'Guest 9');
/*!40000 ALTER TABLE `stay` ENABLE KEYS */;

-- Dumping structure for table bobobox.stayroom
CREATE TABLE IF NOT EXISTS `stayroom` (
  `id` int NOT NULL AUTO_INCREMENT,
  `stay_id` int NOT NULL,
  `room_id` int NOT NULL,
  `date` date NOT NULL,
  PRIMARY KEY (`id`),
  KEY `STAY_ROOM_STAY` (`stay_id`),
  KEY `STAY_ROOM_ROOM` (`room_id`),
  CONSTRAINT `STAY_ROOM_ROOM` FOREIGN KEY (`room_id`) REFERENCES `room` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `STAY_ROOM_STAY` FOREIGN KEY (`stay_id`) REFERENCES `stay` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- Dumping data for table bobobox.stayroom: ~0 rows (approximately)
/*!40000 ALTER TABLE `stayroom` DISABLE KEYS */;
INSERT INTO `stayroom` (`id`, `stay_id`, `room_id`, `date`) VALUES
	(1, 1, 16, '2020-01-11'),
	(2, 1, 16, '2020-01-12'),
	(3, 2, 7, '2020-01-10'),
	(4, 3, 17, '2020-01-14'),
	(5, 3, 17, '2020-01-15'),
	(6, 4, 2, '2020-01-10'),
	(7, 4, 2, '2020-01-11'),
	(8, 5, 10, '2020-01-10'),
	(9, 6, 15, '2020-01-19'),
	(12, 8, 1, '2020-01-09'),
	(13, 9, 1, '2020-01-20'),
	(14, 9, 1, '2020-01-21'),
	(15, 10, 14, '2020-01-08'),
	(16, 10, 14, '2020-01-09'),
	(17, 10, 14, '2020-01-10');
/*!40000 ALTER TABLE `stayroom` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
