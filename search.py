from flask import jsonify
from http import HTTPStatus

def searchTotalRoom(conn, data):
  if 'checkin_date' not in data or 'checkout_date' not in data:
    return jsonify({
        'result': None
        }), HTTPStatus.BAD_REQUEST

  try:
    with conn.cursor() as cursor:
      sql = 'SELECT ' + \
          'room.hotel_id, hotel.hotel_name, COUNT(room.id) AS available_room ' + \
          'FROM room ' + \
          'LEFT JOIN hotel on room.hotel_id = hotel.id ' + \
          'WHERE room.id NOT IN ( ' + \
          'SELECT DISTINCT room_id FROM reservation ' + \
          'WHERE reservation.checkin_date ' + \
          'BETWEEN %s AND DATE_SUB(%s, INTERVAL 1 DAY) OR ' + \
          'reservation.checkout_date ' + \
          'BETWEEN DATE_ADD(%s, INTERVAL 1 DAY) AND %s ' + \
          ') AND room.room_status = 1 ' + \
          'GROUP BY room.hotel_id'
      cursor.execute(sql, (data['checkin_date'], data['checkout_date'],
          data['checkin_date'], data['checkout_date']))
      result = cursor.fetchall()
    return jsonify({
        'result': result
        }), HTTPStatus.OK
  except:
    print('ERROR: Search total')
    return jsonify({
        'result': None
        }), HTTPStatus.INTERNAL_SERVER_ERROR

def searchListRoom(conn, data):
  if 'checkin_date' not in data or 'checkout_date' not in data:
    return jsonify({
        'result': None
        }), HTTPStatus.BAD_REQUEST

  try:
    with conn.cursor() as cursor:
      sql = 'SELECT ' + \
          'room.id, room.hotel_id, room.room_number, hotel.hotel_name ' + \
          'FROM room ' + \
          'LEFT JOIN hotel on room.hotel_id = hotel.id ' + \
          'WHERE room.id NOT IN ( ' + \
          'SELECT DISTINCT room_id FROM reservation ' + \
          'WHERE reservation.checkin_date ' + \
          'BETWEEN %s AND DATE_SUB(%s, INTERVAL 1 DAY) OR ' + \
          'reservation.checkout_date ' + \
          'BETWEEN DATE_ADD(%s, INTERVAL 1 DAY) AND %s ' + \
          ') AND room.room_status = 1'
      cursor.execute(sql, (data['checkin_date'], data['checkout_date'],
          data['checkin_date'], data['checkout_date']))
      result = cursor.fetchall()

    hotel_name = {}
    hotel_room = {}
    for room in result:
      hotel_id = room['hotel_id']
      if hotel_id not in hotel_name:
        hotel_name[hotel_id] = room['hotel_name']
        hotel_room[hotel_id] = []

      del room['hotel_id']
      del room['hotel_name']
      hotel_room[hotel_id].append(room)

    result = []
    for id, name in hotel_name.items():
      result.append({
          'hotel_id': id,
          'hotel_name': name,
          'rooms': hotel_room[id]
      })

    return jsonify({
        'result': result
        }), HTTPStatus.OK
  except:
    print('ERROR: Search list')
    return jsonify({
        'result': None
        }), HTTPStatus.INTERNAL_SERVER_ERROR
