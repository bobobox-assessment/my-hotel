# Bobobox Backend Assessment Test

## Database Configuration

Konfigurasi database dapat disimpan di file `db.conf`

```
<host>:<port>
<user>:<password>
```

## How to run

```python
python bobobox.py
```

## How to run with docker

```
./tools/dockerfile/deployment/build
./tools/dockerfile/deployment/run
```

## Endpoint

### Hotel

- GET `/hotel` - return list of hotel
- POST `/hotel` - create new hotel
- PUT `/hotel` - update hotel
- DELETE `/hotel` - delete hotel

```
# POST /hotel
# BODY json

{
  "hotel_name": "Hotel 1",
  "address": "jalan hotel 1"
}

# PUT /hotel
# BODY json

{
  "id": 1,
  "hotel_name": "Hotel 1",
  "address": "jalan update hotel 1"
}

# DELETE /hotel
# BODY json

{
  "id": 1
}
```

### Room

- GET `/room` - return list of hotel room per hotel
- POST `/room` - create new hotel room
- PUT `/room` - update hotel room
- DELETE `/room` - delete hotel room

```
# POST /room
# BODY json

{
  "hotel_id": 9999,
  "room_number": "123"
}

# PUT /room
# BODY json

{
  "id": 12,
  "hotel_id": 9999,
  "room_number": "123",
  "room_status": 2
}

# DELETE /room
# BODY json

{
  "id": 12
}
```

### Search Available Room

- POST `/search` - return number of available rooms per hotel
- POST `/search/list` - return list of available rooms per hotel

```
# POST /search
# POST /search/list
# BODY json

{
  "checkin_date": "2020-01-10",
  "checkout_date": "2020-01-20"
}
```

### Reservation

- GET `/reservation` - return reservation list
- POST `/reservation` - create new reservation room
- PUT `/reservation` - update reservation room
- DELETE `/reservation` - delete reservation room

```
# POST /reservation
# BODY json

{
  "order_id": 1001,
  "hotel_id": 3,
  "room_id": 12,
  "customer_name": "Customer 1",
  "guest_name": "Guest A",
  "checkin_date": "2020-01-15",
  "checkout_date": "2020-01-17"
}

# PUT /reservation
# BODY json

{
  "id": 15,
  "order_id": 1001,
  "hotel_id": 3,
  "room_id": 12,
  "customer_name": "Customer 1",
  "guest_name": "Guest A",
  "checkin_date": "2020-01-10",
  "checkout_date": "2020-01-17"
}

# DELETE /reservation
# BODY json

{
  "id": 15
}
```
