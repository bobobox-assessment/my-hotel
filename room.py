from flask import jsonify
from http import HTTPStatus

def createRoom(conn, data):
  if 'hotel_id' not in data or 'room_number' not in data:
    return jsonify({
        'result': None
        }), HTTPStatus.BAD_REQUEST

  try:
    with conn.cursor() as cursor:
      sql = 'INSERT INTO `room` (`hotel_id`, `room_number`) VALUES (%s, %s)'
      cursor.execute(sql, (data['hotel_id'], data['room_number']))
      conn.commit()
      room_id = cursor.lastrowid
    return jsonify({
        'result': 'Success',
        'id': room_id
        }), HTTPStatus.OK
  except:
    print('ERROR: Create room')
    return jsonify({
        'result': None
        }), HTTPStatus.INTERNAL_SERVER_ERROR

def readRoom(conn):
  try:
    with conn.cursor() as cursor:
      sql = 'SELECT ' + \
          'room.id, room.hotel_id, hotel.hotel_name, ' + \
          'room.room_number, room.room_status ' + \
          'FROM room LEFT JOIN hotel ' + \
          'ON room.hotel_id = hotel.id'
      cursor.execute(sql)
      result = cursor.fetchall()

    hotel_name = {}
    hotel_room = {}
    for room in result:
      hotel_id = room['hotel_id']
      if hotel_id not in hotel_name:
        hotel_name[hotel_id] = room['hotel_name']
        hotel_room[hotel_id] = []

      del room['hotel_id']
      del room['hotel_name']
      hotel_room[hotel_id].append(room)

    result = []
    for id, name in hotel_name.items():
      result.append({
          'hotel_id': id,
          'hotel_name': name,
          'rooms': hotel_room[id]
      })

    return jsonify({
        'result': result
        }), HTTPStatus.OK
  except:
    print('ERROR: Read room')
    return jsonify({
        'result': None
        }), HTTPStatus.INTERNAL_SERVER_ERROR

def updateRoom(conn, data):
  if 'id' not in data or 'hotel_id' not in data or \
      'room_number' not in data or 'room_status' not in data:
    return jsonify({
        'result': None
        }), HTTPStatus.BAD_REQUEST

  try:
    with conn.cursor() as cursor:
      sql = 'UPDATE room ' + \
          'SET hotel_id = %s, room_number = %s , room_status = %s ' + \
          'WHERE id = %s'
      cursor.execute(sql, (data['hotel_id'], data['room_number'],
          data['room_status'], data['id']))
      conn.commit()
    return jsonify({
        'result': 'Success'
        }), HTTPStatus.OK
  except:
    print('ERROR: Update room')
    return jsonify({
        'result': None
        }), HTTPStatus.INTERNAL_SERVER_ERROR

def deleteRoom(conn, data):
  if 'id' not in data:
    return jsonify({
        'result': None
        }), HTTPStatus.BAD_REQUEST

  try:
    with conn.cursor() as cursor:
      sql = 'DELETE FROM stayroom ' + \
          'WHERE room_id = %s'
      cursor.execute(sql, (data['id']))
      conn.commit()

      sql = 'DELETE FROM stay ' + \
          'WHERE room_id = %s'
      cursor.execute(sql, (data['id']))
      conn.commit()

      sql = 'DELETE FROM reservation ' + \
          'WHERE room_id = %s'
      cursor.execute(sql, (data['id']))
      conn.commit()

      sql = 'DELETE FROM room ' + \
          'WHERE id = %s'
      cursor.execute(sql, (data['id']))
      conn.commit()
    return jsonify({
        'result': 'Success'
        }), HTTPStatus.OK
  except:
    print('ERROR: Delete room')
    return jsonify({
        'result': None
        }), HTTPStatus.INTERNAL_SERVER_ERROR
