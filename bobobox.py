import db
import hotel
import pymysql.cursors
import reservation
import room
import search

from flask import Flask, request
from http import HTTPStatus

if __name__ == '__main__':
  # get database config
  config = db.getConfig()

  if config is None:
    print('No database config at db.conf')
    exit(1)

  # initiate Flask
  app = Flask('Bobobox Assessment')

  # connect database
  mysql = pymysql.connect(
      host=config[0],
      port=int(config[1]),
      user=config[2],
      password=config[3],
      db='bobobox',
      cursorclass=pymysql.cursors.DictCursor
  )

  @app.route('/')
  def homeRoute():
    return 'Bobobox Assessment is Online', HTTPStatus.OK

  @app.route('/hotel', methods=[ 'GET', 'POST', 'PUT', 'DELETE' ])
  def hotelRoute():
    if request.method == 'POST':
      return hotel.createHotel(mysql, request.json)
    elif request.method == 'PUT':
      return hotel.updateHotel(mysql, request.json)
    elif request.method == 'DELETE':
      return hotel.deleteHotel(mysql, request.json)
    else:
      return hotel.readHotel(mysql)

  @app.route('/room', methods=[ 'GET', 'POST', 'PUT', 'DELETE' ])
  def roomRoute():
    if request.method == 'POST':
      return room.createRoom(mysql, request.json)
    elif request.method == 'PUT':
      return room.updateRoom(mysql, request.json)
    elif request.method == 'DELETE':
      return room.deleteRoom(mysql, request.json)
    else:
      return room.readRoom(mysql)

  @app.route('/reservation', methods=[ 'GET', 'POST', 'PUT', 'DELETE' ])
  def reservationRoute():
    if request.method == 'POST':
      return reservation.createReservation(mysql, request.json)
    elif request.method == 'PUT':
      return reservation.updateReservation(mysql, request.json)
    elif request.method == 'DELETE':
      return reservation.deleteReservation(mysql, request.json)
    else:
      return reservation.readReservation(mysql)

  @app.route('/search', methods=[ 'POST' ])
  def searchRoute():
    return search.searchTotalRoom(mysql, request.json)

  @app.route('/search/list', methods=[ 'POST' ])
  def searchListRoute():
    return search.searchListRoom(mysql, request.json)

  # run Flask app
  app.run(host='0.0.0.0', port=8081)

mysql.close()
